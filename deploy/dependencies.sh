#!/bin/sh

set -eu

# Add python pip and bash
apk add bash py-pip

# Install docker compose via pip
pip install --no-cache-dir docker-compose
docker-compose -v
