from os import environ as env

from .base_settings import *

# Google Map API for django-geoposition
GEOPOSITION_GOOGLE_MAPS_API_KEY = env.get('GEOPOSITION_GOOGLE_MAPS_API_KEY')

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = env.get('SECRET_KEY')

ALLOWED_HOSTS.append(env.get('APP_HOST_NAME', None))

GOOGLE_API_KEY = env.get('GOOGLE_API_KEY')

DATABASES = {
    "default": {
        "ENGINE": "django.db.backends.postgresql",
        "HOST": env.get("DB_HOST"),
        "NAME": env.get("DB_NAME"),
        "PASSWORD": env.get("DB_PASSWORD"),
        "PORT": env.get("DB_PORT"),
        "USER": env.get("DB_USER")
    },
}
